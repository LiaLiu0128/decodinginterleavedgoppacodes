# Copyright @ Hedongliang LIU (lia.liu@tum.de)
# December, 2018
# run by load("GoppaCodes_example.sage") in a Sage bash

"""
an example of using GoppaCode class to construct Goppa codes 
and using decoders classes to decode interleaved Goppa codes 
or uniquely decode single received word.

"""
load("Goppa_code.py")
# ======== code construction =========================
q = 2
m = 8
r = 7

F_base = GF(q)
F_ext = GF(q^m)
FE = RFFE(F_ext, F_base)
PR = PolynomialRing(F_ext, 'x')
x = PR.gen()
L = F_ext.list()[1:]
g = x^7 + x + 1# PR.irreducible_element(r) # x^3 + x + 1

GC = GoppaCode(L, g, FE)
RS = GC._RS()

n = GC.length()
k_G = GC.dimension()
k_R = RS.dimension()
d_G = GC.minimum_distance()
d_R = RS.minimum_distance()
print("Goppa degree r: %s" % GC._r())
print(GC)
print(RS)

H = GC.parity_check_matrix()

GC2 = GoppaCode(L, g^2, FE) # wild goppa code
RS2 = GC2._RS()
print("wild Goppa degree: %s" % GC2._r())
# print(GC2)
# print(RS2)
# H2 = GC2.parity_check_matrix()

# ========test decoders for interleaved codes =========================
print("======== Interleaved ======== ")
# -------- 1. encoding Interleaved GC code --------

l = 7 # interleaving order <= r-1, greater than this value, no improvements on decoding radius anymore
print("Interleaving order l: %s" % l)

cw = []
for i in range(l):
	u = random_vector(F_base, k_G)
	# us.append(u)
	c = GC.encode(u)
	cw.append(c)
C = matrix(F_base, cw)

# -------- 2. add full-rank error matrix --------
t = floor(l/(l+1)*GC._r())
e_loc = sample(range(n), t) # [1,23]
E = matrix(F_ext, l, n)
E_cols = E.columns()
for loc in e_loc:
	while E_cols[loc] == 0:
		E_cols[loc] = random_vector(F_ext, l) # error is generated in extension field
E = column_matrix(F_ext, E_cols)
R = C + E

# # ---- Error matrix for wild goppa ----
t_wild = floor(l/(l+1)*GC2._r())
e_wild_loc = sample(range(n), t_wild)
E = matrix(F_base, l, n)
E_cols = E.columns()
for loc in e_wild_loc:
	while E_cols[loc] == 0:
		E_cols[loc] = random_vector(F_ext, l) # error is generated in extension field
E_wild = column_matrix(F_ext, E_cols)
R_wild = C + E_wild

# -------- 3. decode --------

C_hat = GC.decode_to_code((R,t), 'IRSDecoder')
print("IRS Decoding %s errors correctly? %s " % (t, C_hat == C))
C_hat2 = GC2.decode_to_code((R_wild,t_wild), 'IRSDecoder')
print("wild IRS Decoding %s errors correctly? %s " % (t_wild, C_hat2 == C))

C_IGC = GC.decode_to_code((R,t), 'IntGoppaSyndromeDecoder') # ==== Interleaved Goppa Syndrome decoding ==== up to r-1
print("IGC Syndrome Decoding %s errors correctly? %s " % (t, C_IGC == C))

C_IGC2 = GC2.decode_to_code((R_wild,t_wild), 'IntGoppaSyndromeDecoder')# ==== Interleaved decode by GC2 ==== up to 2r-1?
print("wild GC2 Syndrome Decoding %s errors correctly? %s " % (t_wild, C_IGC2 == C))

# ========test unique decoders =========================
print("======== Single word Decoders ========" )
# -------- codeword and error generate --------
t = floor(GC2._r()/2)
e_loc = sample(range(n),t) #[1,6]
# print(e_loc)
e = vector(F_ext, n)
for j in e_loc:
	e[j] = F_ext.random_element() # error is generated in extension field
# print(e)
c = GC.encode(random_vector(F_base, k_G))
r = c + e
# -------- decode --------
c_hat = GC.decode_to_code(r) # ==== Patterson Decoder ==== decode up to r errors
print("GC Patterson decode %s errors correctly? %s" % (t, c==c_hat))

S = GC.decode_to_code(r, 'GeneralizedPattersonDecoder') # ==== Generalized Patterson Decoder ==== decode up to 2/3 r errors
# print(S)
for c_hat in S:
	print("p-ary PattersonDecoder decode %s errors correctly? %s" % (t, c == c_hat))

c_hat2 = GC2.decode_to_code((r,t), 'GoppaSyndromeDecoder') # ==== g2 Goppa syndrome decoder, decode by GC2==== up to r errors
print("GC2 Syndrome decode %s errors correctly? %s" % (t, c==c_hat2))

c_hat_RS = GC2.decode_to_code(r, "RSSuperCodeDecoder")# ==== RS supercode Decoder ==== decode up to floor(r/2) errors
print("RS2 supercode decode %s errors correctly? %s" % (t, c == c_hat_RS))

c_RS = GC2.decode_to_code((r,t), "RSSyndromeDecoder")# ==== RS syndrome-based Decoder ==== decode up to floor(r/2) errors
print("RS2 syndrome-based decode %s errors correctly? %s" % (t, c == c_RS))
