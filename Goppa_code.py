from sage.coding.linear_code import AbstractLinearCode
from sage.coding.encoder import Encoder
from sage.coding.decoder import Decoder
from sage.rings.finite_rings.finite_field_constructor import FiniteField as GF
from sage.coding.relative_finite_field_extension import RelativeFiniteFieldExtension as RFFE
from Johan_lib import *
"""
---- v8 = final ----
difference from v7:
change the solving LSE method to lattice reduction in IntGoppaSyndromedecoder.decode_to_code() since the latter is robuster.
"""

def ext_Euclidean(a, b):
	"""
	extended Euclidean algorithm (a.k.a., extended GCD algorithm)
	Input: a,b: two univariate polynomial
	Output: g: the gcd of a and b
			u,v: such that au + bv = g
			a_ori, b_ori: such that a = g * a_ori, b = g * b_ori
	"""
	# initialization
	rose = [a, b]
	salvia = [a.parent(1), a.parent(0)]
	tulip = [a.parent(0), a.parent(1)]
	i = 1
	# iteration:
	while (rose[i-1].degree() > floor(b.degree()/2) or salvia[i-1].degree()>floor((b.degree()-1)/2)):
		quo, r = rose[i-1].quo_rem(rose[i])
		rose.append(r)
		salvia.append(salvia[i-1] - quo * salvia[i])
		tulip.append(tulip[i-1] - quo * tulip[i])
		i = i + 1
	g = rose[i-1]
	u = salvia[i-1]
	v = tulip[i-1]
	a_ori = (-1)**(i-1) * tulip[i]
	b_ori = (-1)**i * salvia[i]
	assert g == a * u + b * v, "identity doesn't hold"
	return g, u, v, a_ori, b_ori

class GoppaCode(AbstractLinearCode):
	_registered_encoders = {}
	_registered_decoders = {}
	def __init__(self, L, poly, FE):
		super(GoppaCode, self).__init__(GF(2), len(L), "CommonEncoder", "PattersonDecoder")
		self._poly = poly
		self._L = L
		self._FE = FE
		for ele in L:
			assert poly(ele) != 0, "Evaluation of Goppa polynomial on the element in L should not be zero."

	def _repr_(self):
		return "[%s,%s,>=%s]_%s Goppa Code" % (self.length(), self.dimension(), self.minimum_distance(), self._F_base().cardinality())

	def __eq__(self, other):
		return(isinstance(other, GoppaCode)
		       and self.generator_matrix()* other.parity_check_matrix().transpose() == 0)

	def _F_ext(self):
		return self._FE.absolute_field()
	def _F_base(self):
		return self._FE.relative_field()
	# def _q(self):
	# 	return self._q#self._F_base().order()
	def _m(self):
		return self._FE.extension_degree()
	def _r(self):
		return self._poly.degree()
	def _x(self):
		return self._poly.parent().gen()
	def minimum_distance(self):
		if self._F_base().order() == 2:
			if self._poly.is_squarefree():
				return (2 * self._r() + 1)
			else:
				return self._RS().minimum_distance()
		else:
			return self._RS().minimum_distance() # self._r() + 1
	def _RS(self):
		"""
		construct the GRS code such that self is a subfield subcode of the GRS
		"""
		L = self._L
		poly = self._poly
		n = self.length()

		k_RS = self.length() - self._r()
		eval_points = self._L

		v = []
		for i in range(n):
			prodi = prod([L[i]-L[j] for j in range(n) if j != i])
			vi = poly(L[i])/prodi
			v.append(vi)
		RS = codes.GeneralizedReedSolomonCode(eval_points, k_RS, v)
		return RS

	def Fext2Fbase(self, ele):
		rep_base = self._FE.relative_field_representation(ele).list()
		return rep_base

	def parity_check_matrix_ext(self):
		m = self._m()
		n = self.length()
		r = self._r()

		L = self._L
		poly = self._poly

		F_ext = self._F_ext()
		F_base = self._F_base()

		g_eva_inv = [poly(ele)**(-1) for ele in L]
		# g_coe_re = poly.list()[1:]
		# g_coe_re.reverse()
		# C = matrix.toeplitz(g_coe_re, [F_ext(0)]*(len(g_coe_re)-1)) # CXY give an equivalent parity check matrix of the same Goppa codes
		X = matrix.vandermonde(L).transpose()[:r,:]
		Y = matrix.diagonal(g_eva_inv)
		H = X * Y
		return H

	def parity_check_matrix(self):
		m = self._m()
		n = self.length()
		r = self._r()
		H = self.parity_check_matrix_ext()
		# extend each symbol in extension field into a vector in base field
		H_G = matrix(self._F_base(), r*m, n)
		for i in range(r):
			for j in range(n):
				H_G[i*m:(i+1)*m, j] = vector(self.Fext2Fbase(H[i,j]))
		return H_G

	def _SyndromeCalculator(self):
		S = [(self._x() - self._L[i]).inverse_mod(self._poly) for i in range(self.length())]
		return S

	# def print_sigma(self, e):
	# 	"""
	# 	This is for testing purpose, should be delete in final version
	# 	show the associated polynomials about e
	# 	"""
	# 	sigma = prod([self._x() - self._L[i] for i in e.nonzero_positions()])
	# 	print('sigma_e: %s' % sigma)
	# 	print('sigma_e_factor: %s' % sigma.factor())
	# 	a0, a1 = split_even_odd(sigma)
	# 	print('a0_e : %s' % a0)
	# 	print('a1_e : %s' % a1)

class GoppaCodeCommonEncoder(Encoder):
	def __init__(self, code):
		super(GoppaCodeCommonEncoder, self).__init__(code)

	def _repr_(self):
		return "Goppa Code common encoder for the %s" % self.code()

	def _latex_(self):
		return "\textnormal{Goppa Code common encoder for the } %s" % self.code()

	def __eq__(self, other):
		return (isinstance(other, GoppaCodeCommonEncoder)
				and self.code() == other.code())

	def generator_matrix(self):
		"""
		return the generator matrix by the most common method
		"""
		H_G = self.code().parity_check_matrix()
		return H_G.right_kernel_matrix()

	def generator_matrix_systematic(self):
		return self.generator_matrix().rref()

	def encode(self, message):
		return message * self.generator_matrix()

class IntGoppaSyndromeDecoder(Decoder):
	def __init__(self, code):
		super(IntGoppaSyndromeDecoder, self).__init__(code, code.ambient_space(), "GoppaCodeCommonEncoder")

	def _repr_(self):
		return "Interleaved Goppa Syndrome Decoder for %s" % self.code()

	def _latex_(self):
		return "\textnormal{Interleaved Goppa Syndrome Decoder for } %s" % self.code()

	def __eq__(self, other):
		return (isinstance(other, IntGoppaSyndromeDecoder)
		        and self.code() == other.code())
	def coeff_mod(self, t):
		"""
		generate a LUT for all deg(g(x))<= i <= t + deg(g)-1:
		i   |   coefficients of x^i mod g(x)
		output: a dictionary with i as keys and coefficients as values
		"""
		g = self.code()._poly
		x = self.code()._x()
		r = g.degree()
		LUT = {}
		for i in range(r, r+t):
			poly = g.parent()(x**i)
			poly_mod = poly.mod(g)
			LUT[i] = poly_mod.list() + [0] * (r-1-poly_mod.degree())
		return LUT
	def shift_right_insert(self, vec, ele):
		"""
		performing shift vec right by 1, and insert ele into vec on the left
		"""
		del vec[-1]
		vec.insert(0,ele)
		return vec

	def each_LSE(self, syn_poly, t):
		"""
		!!! this is NOT the same as LSE in GoppaSyndromeDecoder !!!
		construct the LSE of Patterson key equation
		return the equations obtained from each syndrome
		input: syn_poly: one syndrome polynomial
				t: number of errors actually occurring, i.e. deg(sigma)
		output: equations coefficients matrix
		"""
		LUT = self.coeff_mod(t)
		r = self.code()._r()
		x = self.code()._x()
		F_ext = self.code()._F_ext()
		# ---- set up whole coefficients matrix
		ncols = t + 1
		nrows = r + t -1
		col = syn_poly.list() + [F_ext(0)] * (nrows - syn_poly.degree())
		M_cols = []
		for i in range(ncols):
			M_cols.append(copy(col))
			col = self.shift_right_insert(col,F_ext(0))
		M = column_matrix(F_ext, M_cols)
		# print(M)
		# print("\n")
		# ---- do mod g(x) modification
		for k in range(r):
			for j in range(ncols):
				try:
					M[k,j] = M[k,j] + sum([M[i,j]*LUT[i][k] for i in range(r, r+t)])
				except IndexError:
					print("Index error: k:%s, j:%s, i:%s" % (k,j,i))
		# print(M)
		# print("\n")
		# ---- return the key equation part in M
		A = M[t:r, 1:]
		T = - M[t:r,0]
		keypart = column_matrix(F_ext, A.columns() + T.columns())
		# print(keypart)
		# input()
		return keypart.rows()

	def int_LSE(self, S, t):
		"""
		construct the LSE of Patterson key equation for interleaved Goppa codes
		return solved sigma
		input: S: a list of syndrome polynomials
			   t: number of errors actually occurring, i.e. deg(sigma)
		output: sigma_x obtained by solving LSE
		"""
		r = self.code()._r()
		x = self.code()._x()
		M = []
		for syn in S:
			M += self.each_LSE(syn, t)
		M = matrix(M)
		sigma = [1] + M.rref()[:t, -1].list()
		sigma_x = sum([sigma[i] * (x**i) for i in range(len(sigma))])

		return sigma_x

	def decode_to_code(self, (word, t)):
		SynCal = matrix(self.code()._SyndromeCalculator())
		F = self.code()._F_ext()
		PR = PolynomialRing(F, 'x')
		g = self.code()._poly
		L = self.code()._L
		# ==== by syndrome-based key equation: sigma * S = omega mod g(x) --------
		S = (word * SynCal.transpose()).list()
		# ==== two ways to find ELP Lambda_x
		# ---- 1. set up LSE to calculate sigma(x)
                """
                this method is not robust compared to lattice reduction:
                when E has zero columns, this method returns failures or wrong codeword even if the number of errors in each row is within unique decoding radius; while lattice reducion always guarantee the decoding.
                when E has no zero columns but does not have full-rank, this method returns more failures than lattice reduction method.

                """
		# sigma_x = self.int_LSE(S, t)

		# # ---- 2. Lattice construction of syndrome-based key equation --------
		l = len(S)
		M = diagonal_matrix(PR, l+1, [g]*(l+1))
		M[0,0] = PR(1)
		for j in range(1, M.ncols()):
			M[0, j] = S[j-1]
		module_weak_popov(M)
		M_rows = M.rows()
		LP_row = [LP(M_rows[i]) for i in range(M.nrows())]#module_all_degrees(M)
		loc0 = LP_row.index(0)
		solu = M_rows[loc0].list()
		sigma_x = copy(solu[0])

		# ==== following are the common steps
		roots = sigma_x.roots(multiplicities = False)

		try:
			errloc = [L.index(root) for root in roots]
			# print(errloc)
		except ValueError:
			# pass
			# raise ValueError("GC: one of roots is not in L")
			return 'failure'
		# ---- error value decoding ----
		sigma_de = sigma_x.derivative()
		C_hat = matrix(self.code()._F_base(), word.rows())
		# C_hat = copy(word)
		for i in range(C_hat.nrows()):
			omega = (sigma_x * S[i]).mod(g)
			for j in errloc:
				try:
					errval = - omega(L[j])/sigma_de(L[j])
				except ZeroDivisionError:
					# pass
					# raise ZeroDivisionError
					return 'failure'
				try:
					C_hat[i, j] += errval
				except ValueError:
					# pass
					# raise ValueError("GC: error value is not in base field")
					return 'failure'
		return C_hat

class IRS_Decoder(Decoder):
	def __init__(self, code):
		super(IRS_Decoder, self).__init__(code, code.ambient_space(),"GoppaCodeCommonEncoder")

	def _repr_(self):
		return "IRS decoder for %s" % self.code()

	def _latex_(self):
		return "\textnormal{IRS decoder for } %s" % self.code()

	def __eq__(self, other):
		return (isinstance(other, IRS_Decoder)
				and self.code() == other.code())
	def shift_right_insert(self, vec, ele):
		"""
		performing shift vec right by 1, and insert ele into vec on the left
		"""
		del vec[-1]
		vec.insert(0,ele)
		return vec

	def RS_LSE(self, syn, t):
		"""
		construct the LSE of RS key equation
		return the equations obtained from each syndrome
		input: syn: symbols list of one syndrome
				t: number of errors actually happening, i.e. deg(Lambda_x)
		output: equations coefficients matrix
		"""
		r = self.code()._r()
		F_ext = self.code()._F_ext()
		ncols =t+1

		# ---- set up whole coefficients matrix
		nrows = r + t -1
		col = syn + [F_ext(0)] * (nrows - len(syn)+1)
		M_cols = []
		for i in range(ncols):
			M_cols.append(copy(col))
			col = self.shift_right_insert(col,F_ext(0))
		M = column_matrix(F_ext, M_cols)

		# ---- return the key equation part in M
		A = M[t:r, 1:]
		T = - M[t:r,0]
		keypart = column_matrix(A.columns() + T.columns())

		return keypart.rows()

	def IRS_LSE(self, S, t):
		"""
		construct the LSE of RS key equation for interleaved codes
		return solved sigma
		input: S: a list of syndromes
		output: sigma obtained by solving LSE
		"""
		l = len(S)
		r = self.code()._r()
		x = self.code()._x()
		M = []
		for syn in S:
			M += self.RS_LSE(syn.list(), t)
		M = matrix(M)
		sigma = [1] + M.rref()[:t, -1].list()
		sigma_x = sum([sigma[i] * (x**i) for i in range(len(sigma))])
		return sigma_x

	def decode_to_code(self, (Rec_Word, t)):
		"""
		based on Lambda * syndrome = Omega mod x^(d-1)
		*** can not decode if evaluation point 0 is one of the error position ***
		Perform collaborative decoding of multiple received words with burst errors
		via lattice reduction or solving LSE
		"""
		RS = self.code()._RS()
		F = RS.base_field()
		PR = PolynomialRing(F, 'x')
		x = self.code()._x()
		H = RS.parity_check_matrix()
		d = H.nrows()+1
		L = RS.evaluation_points().list()

		# -------- by syndrome-based key equation: lambda * S = omega mod x^(d-1) --------
		synd = []
		S = Rec_Word.change_ring(F) * H.transpose()
		synd = S.rows() # get the syndrome of each word
		s_x = [sum([s[i] * (x**i) for i in range(len(s))]) for s in synd]

		# ==== two ways to find ELP Lambda_x
		# ---- 1. by solving LSE of syndromes
		Lambda_x = self.IRS_LSE(synd, t)

		# # ---- 2. Lattice construction of syndrome-based key equation --------
		# G = x**(d-1)
		# l = len(s_x)
		# M = diagonal_matrix(PR, l+1, [G]*(l+1))
		# M[0,0] = PR(1)
		# for j in range(1, M.ncols()):
		# 	M[0, j] = s_x[j-1]
		# module_weak_popov(M)
		# M_rows = M.rows()
		# LP_row = [LP(M_rows[i]) for i in range(M.nrows())]#module_all_degrees(M)
		# loc0 = LP_row.index(0)
		# solu = M_rows[loc0].list()
		# Lambda_x = solu[0]

		# ==== following are common steps
		roots = Lambda_x.roots(multiplicities = False)
		try:
			errloc = [L.index(root) for root in roots]
		except ValueError:
			return Rec_Word# raise ValueError("RS: one of roots is not in L")
		L_inv = [ele**(-1) for ele in L if ele != 0]
		if 0 in L:
			L_inv.insert(L.index(0), F(0))
		err_loc = [L_inv.index(root) for root in roots]
		# -------- Calculate the error values at error positions --------
		F_base = self.code()._F_base()
		ColMul = RS.parity_column_multipliers()
		Lambda_de = Lambda_x.derivative()
		# C_hat = []
		# R_rows = Rec_Word.rows()
		C_hat = matrix(F_base, Rec_Word.rows())
		for ind in range(C_hat.nrows()):
			Omega_x = (Lambda_x * s_x[ind]).mod(x**(d-1)) #Omega[ind]#
			# c = copy(R_rows[ind])
			for i in err_loc:
				try:
					val = (L[i]/ColMul[i]) * (Omega_x(L_inv[i])/Lambda_de(L_inv[i]))
				except ZeroDivisionError:
					return 'failure'# print("RS: divides 0 error")
				try:
					C_hat[ind,i] = Rec_Word[ind,i] + (val)
				except ValueError:
					return 'failure'# print("RS: error value is not in base field")
			# C_hat.append(c)
		# C_mat  = matrix(F_base, C_hat)
		return C_hat

class GoppaSyndromeDecoder(Decoder):
	def __init__(self, code):
		super(GoppaSyndromeDecoder, self).__init__(code, code.ambient_space(), "GoppaCodeCommonEncoder")

	def _repr_(self):
		return "Goppa Syndrome Decoder for %s" % self.code()

	def _latex_(self):
		return "\textnormal{Goppa Syndrome Decoder for } %s" % self.code()

	def __eq__(self, other):
		return (isinstance(other, GoppaSyndromeDecoder)
		        and self.code() == other.code())
	def coeff_mod(self, t):
		"""
		generate a LUT for all deg(g(x))<= i <= t + deg(g)-1:
		i   |   coefficients of x^i mod g(x)
		output: a dictionary with i as keys and coefficients as values
		"""
		g = self.code()._poly
		x = self.code()._x()
		r = g.degree()
		LUT = {}
		for i in range(r, r+t):
			poly = g.parent()(x**i)
			poly_mod = poly.mod(g)
			LUT[i] = poly_mod.list() + [0] * (r-1-poly_mod.degree())
		return LUT
	def shift_right_insert(self, vec, ele):
		"""
		performing shift vec right by 1, and insert ele into vec on the left
		"""
		del vec[-1]
		vec.insert(0,ele)
		return vec

	def LSE(self, syn_poly, t):
		"""
		construct the LSE of Goppa key equation
		return matrices M_up and M_lo which are respectively the upper and lower part of M s.t. M*lambda=omega
		input: syndrome polynomial
				t: number of errors actually occurring, i.e. deg(sigma)
		output: sigma obtained by solving LSE
		"""
		LUT = self.coeff_mod(t)
		r = self.code()._r()
		x = self.code()._x()
		F_ext = self.code()._F_ext()

		# ---- set up whole coefficients matrix
		ncols = t+1
		nrows = r + t -1
		col = syn_poly.list() + [F_ext(0)] * (nrows - syn_poly.degree())
		M_cols = []
		for i in range(ncols):
			M_cols.append(copy(col))
			col = self.shift_right_insert(col,F_ext(0))
		M = column_matrix(F_ext, M_cols)

		# ---- do mod g(x) modification
		for k in range(r):
			for j in range(ncols):
				try:
					M[k,j] = M[k,j] + sum([M[i,j]*LUT[i][k] for i in range(r, r+ncols-1)])
				except IndexError:
					print("Index error: k:%s, j:%s, i:%s" % (k,j,i))

		# ---- solve the LSE by doing Gaussian elimination on re-organized M
		A = M[t: r, 1:]
		T = -M[t: r, 0]
		AT = column_matrix(F_ext, A.columns()+T.columns())
		solu = AT.rref()[:t,-1].list()
		sigma = vector([1] + solu)
		sigma_x = sum([sigma[i] * (x**i) for i in range(len(sigma))])
		return sigma_x

	def decode_to_code(self, (word, num_err)):
		SynCal = self.code()._SyndromeCalculator()

		s = word.dot_product(vector(SynCal))
		F = self.code()._F_ext()
		PR = PolynomialRing(F, 'x')
		g = self.code()._poly
		L = self.code()._L

		# ==== two ways to find ELP sigma(x)
		# ---- 1. construct LSE to calculate sigma_x ----
		sigma_x = self.LSE(s, num_err)

		# # ---- 2. lattice reduction to calculate sigma_x ----
		# M = matrix(PR, [[1, s], [0, g]])
		# module_weak_popov(M)
		# M_rows = M.rows()
		# LP_row = [LP(M_rows[i]) for i in range(M.nrows())]
		# loc0 = LP_row.index(0)
		# solu = M_rows[loc0].list()
		# sigma_x = solu[0]

		# ==== following are the common steps
		roots = sigma_x.roots(multiplicities = False)
		omega_x = (s*sigma_x).mod(g) #solu[1]
		errloc = [L.index(root) for root in roots]
		# print('GC: error locations are %s' % errloc)
		c_hat = vector(F, word.list())
		sigma_de = sigma_x.derivative()
		for i in errloc:
			try:
				errval = - omega_x(L[i])/sigma_de(L[i])
			except ZeroDivisionError:
				# print("GC: division zero error")
				return 'failure'
			try:
				c_hat[i] += errval
			except ValueError:
				# print("GC: error value not in base field")
				return 'failure'
		return c_hat
class RSSyndromeDecoder(Decoder):
	def __init__(self, code):
		super(RSSyndromeDecoder, self).__init__(code, code.ambient_space(),"GoppaCodeCommonEncoder")

	def _repr_(self):
		return "RS syndrome-based decoder for %s" % self.code()

	def _latex_(self):
		return "\textnormal{RS syndrome-based decoder for } %s" % self.code()

	def __eq__(self, other):
		return (isinstance(other, RSSyndromeDecoder)
				and self.code() == other.code())
	def shift_right_insert(self, vec, ele):
		"""
		performing shift vec right by 1, and insert ele into vec on the left
		"""
		del vec[-1]
		vec.insert(0,ele)
		return vec

	def RS_LSE(self, syn, t):
		"""
		!!! this is not the same as RS_LSE in IRSDecoder!!!
		construct the LSE of RS key equation
		return the equations obtained from each syndrome
		input: syn: symbols list of one syndrome
				t: number of errors actually happening, i.e. deg(Lambda_x)
		output: equations coefficients matrix
		"""
		r = self.code()._r()
		F_ext = self.code()._F_ext()
		x = self.code()._x()
		ncols =t+1

		# ---- set up whole coefficients matrix
		nrows = r + t -1
		col = syn + [F_ext(0)] * (nrows - len(syn)+1)
		M_cols = []
		for i in range(ncols):
			M_cols.append(copy(col))
			col = self.shift_right_insert(col,F_ext(0))
		M = column_matrix(F_ext, M_cols)

		# ---- return the key equation part in M
		A = M[t:r, 1:]
		T = - M[t:r,0]
		keypart = column_matrix(A.columns() + T.columns())
		sigma = [1] + keypart.rref()[:t, -1].list()
		sigma_x = sum([sigma[i] * (x**i) for i in range(len(sigma))])
		return sigma_x

	def decode_to_code(self, (Rec_Word, t)):
		"""
		based on Lambda * syndrome = Omega mod x^(d-1)
		*** can not decode if evaluation point 0 is one of the error position ***
		Perform collaborative decoding of multiple received words with burst errors
		via lattice reduction or solving LSE
		"""
		RS = self.code()._RS()
		F = RS.base_field()
		PR = PolynomialRing(F, 'x')
		x = self.code()._x()
		H = RS.parity_check_matrix()
		d = H.nrows()+1
		L = RS.evaluation_points().list()

		s = Rec_Word * H.transpose()
		s_x = sum([s[i] * (x**i) for i in range(len(s))])
		# ==== two ways to find ELP Lambda(x)
		# ---- 1. construct LSE to calculate Lambda_x ----
		Lambda_x = self.RS_LSE(s.list(), t)

		# # ---- 2. lattice reduction to calculate sigma_x ----
		# M = matrix(PR, [[1, s_x], [0, x**(d-1)]])
		# module_weak_popov(M)
		# M_rows = M.rows()
		# LP_row = [LP(M_rows[i]) for i in range(M.nrows())]
		# loc0 = LP_row.index(0)
		# solu = M_rows[loc0].list()
		# Lambda_x = solu[0]

		# ==== following are common steps
		roots = Lambda_x.roots(multiplicities = False)
		try:
			errloc = [L.index(root) for root in roots]
		except ValueError:
			raise ValueError("RS: one of roots is not in L")
			return False
		L_inv = [ele**(-1) for ele in L if ele != 0]
		if 0 in L:
			L_inv.insert(L.index(0), F(0))
		err_loc = [L_inv.index(root) for root in roots]
		# print(errloc)
		# -------- Calculate the error values at error positions --------
		F_base = self.code()._F_base()
		ColMul = RS.parity_column_multipliers()
		Lambda_de = Lambda_x.derivative()
		Omega_x = (Lambda_x * s_x).mod(x**(d-1)) #Omega[ind]#
		c = vector(F, Rec_Word.list())
		for i in err_loc:
			try:
				val = (L[i]/ColMul[i]) * (Omega_x(L_inv[i])/Lambda_de(L_inv[i]))
			except ZeroDivisionError:
				print("RS: zerodiviser")
				return False
			try:
				c[i] = c[i] + (val)
			except ValueError:
				print("RS: value not in base field")
				return False
		return c

class RSSuperCodeDecoder(Decoder):
	"""
	RS super code decoder: regard the goppa code as a RS code
	"""
	def __init__(self, code):
		super(RSSuperCodeDecoder, self).__init__(code, code.ambient_space(),"GoppaCodeCommonEncoder")

	def _repr_(self):
		return "RS super code decoder for %s" % self.code()

	def _latex_(self):
		return "\textnormal{RS super code decoder for } %s" % self.code()

	def __eq__(self, other):
		return (isinstance(other, RSSuperCodeDecoder)
				and self.code() == other.code())

	def decode_to_code(self, word):
		try:
			word_decoded = self.code()._RS().decode_to_code(word)
		except:
			return False
		return word_decoded


	def decoding_radius(self):
		# distance of the corresponding RS code is r+1
		return floor(self.code()._r()/2)

class PattersonDecoder(Decoder):
	"""
	Patterson's decoding algorithm (1975)
	with extended Euclidean algorithm
	Only works for binary square-free Goppa codes
	"""
	def __init__(self, code):
		super(PattersonDecoder, self).__init__(code, code.ambient_space(),"GoppaCodeCommonEncoder")

	def _repr_(self):
		return "Patterson decoder for %s" % self.code()

	def _latex_(self):
		return "\textnormal{Patterson decoder for } %s" % self.code()

	def __eq__(self, other):
		return (isinstance(other, PattersonDecoder)
				and self.code() == other.code())
	def split_even_odd(self, p):
		"""
		From:
		Roering, Christopher, "Coding Theory-Based Cryptography: McEliece Cryptosystems in Sage" (2013). Honors Theses, 1963-2015. 17.
		https://digitalcommons.csbsju.edu/honors_theses/17

		split polynomial p into even part p0 and odd part p1 such that p(z) = p0(z)^2 + zp1(z)^2
		"""
		F = p.parent()
		p0 = F([sqrt(c) for c in p.list()[0::2]])
		p1 = F([sqrt(c) for c in p.list()[1::2]])
		return p0, p1

	# def sqrt_mod(p, g):
	# 	"""
	# 	calculate the square root of a polynomial p in module of polynomial g
	# 	"""
	# 	p0, p1 = self.split_even_odd(p) # square roots of even and odd degree parts respectively
	# 	g0, g1 = self.split_even_odd(g) # help to calculate sqrt(x)
	# 	w = (g0 * g1.inverse_mod(g)).mod(g) # sqrt(x)
	# 	return p0 + w * p1

	def decode_to_code(self, word):
		F_base = self.code()._F_base()
		if F_base is not GF(2):
			raise ValueError("Patterson decoder works only for Goppa code in characteristic 2")
		g = self.code()._poly
		if not g.is_squarefree():
			raise ValueError("Patterson decoder works only for square-free Goppa codes.")
		SynCal = self.code()._SyndromeCalculator()
		s = word.dot_product(vector(SynCal))
		if s == 0:
			print("The word is a codeword of %s." % self.code())
		else:
			T = s.inverse_mod(self.code()._poly)
			if T == self.code()._x():
				sigma = T

			else:
				T0, T1 = self.split_even_odd(T + self.code()._x())
				g0, g1 = self.split_even_odd(self.code()._poly)
				w = (g0 * g1.inverse_mod(self.code()._poly)).mod(self.code()._poly)
				R = T0 + w * T1
				a0, a1, v, b0, b1 = ext_Euclidean(R, self.code()._poly)

				sigma = a0*a0 + self.code()._x() * a1 * a1
			roots = sigma.roots(multiplicities=false)
			L = self.code()._L
			errloc = [L.index(root) for root in roots]
			c_hat = copy(word)
			for i in errloc:
				c_hat[i] += 1
		return c_hat


	def decoding_radius(self):
		n = self.code().length()
		m = self.code()._m
		r = self.code()._poly.degree()
		F_base = self.code()._F_base()
		if F_base == GF(2):
			return r
		else:
			return floor(r/2)

class GeneralizedPattersonDecoder(Decoder):
	"""
	Probabilistic Generalized Patterson Decoder, a.k.a., p-ary Patterson decoder (2013, Barrato)
	with Lattice-reduction algorithm (using the library implemented by Johan S. R. Nielsen)
	"""
	def __init__(self, code):
		super(GeneralizedPattersonDecoder, self).__init__(code, code.ambient_space(),"GoppaCodeCommonEncoder")

	def _repr_(self):
		return "Probabilistic Generalized Patterson Decoder for %s" % self.code()

	def _latex_(self):
		return "\textnormal{Probabilistic Generalized Patterson Decoder for } %s" % self.code()

	def __eq__(self, other):
		return (isinstance(other, GeneralizedPattersonDecoder)
				and self.code() == other.code())

	def _p_th_root_x(self, p):
		"""
		return the p-th root of x mod g
		"""
		g = self.code()._poly
		PR = g.parent()
		g0 = PR([c.nth_root(p) for c in g.list()[0::2]])
		g1 = PR([c.nth_root(p) for c in g.list()[1::2]])
		return (-g0 * g1.inverse_mod(g)).mod(g)

	def _p_th_root_poly(self, p, poly):
		"""
		return the p-th root of a polynomial
		"""
		PR = poly.parent()
		u = []
		for i in range(p):
			u.append(PR([c.nth_root(p) for c in poly.list()[i::p]]))
		w = self._p_th_root_x(p)
		root = sum([w**i * u[i] for i in range(p)])
		return root

	def decode_to_code(self, word):
		F = self.code()._F_ext()
		F_base = self.code()._F_base()
		p = F_base.order()
		x = self.code()._x()
		g = self.code()._poly
		if not g.is_squarefree():
			print("GeneralizedPattersonDecoder works only for square-free Goppa codes.")
			return []
		r = g.degree()
		PR = g.parent()
		L = self.code()._L
		SynCal = self.code()._SyndromeCalculator()
		s = word.dot_product(vector(SynCal))
		S = [] # the set of possible codewords
		if s == 0:
			print("The word is a codeword of %s." % self.code())
			S.append(word) # S-list decoding like
		else:
			s_inv = s.inverse_mod(self.code()._poly)
			for phi in range(1, p): # ---- guess the correct scale factor phi
				u = []
				v = []
				for k in range(1, p):
					u_x = (x**k - phi * k * x**(k-1) * s_inv ).mod(g)
					v_x = self._p_th_root_poly(p, u_x)
					u.append(u_x)
					v.append(v_x)

				# ---- build the lattice basis and reduce it to weak-popov form ----
				A_phi = diagonal_matrix([PR(1)]*p)
				A_phi[0,0] = g
				for i in range(1,A_phi.nrows()):
					A_phi[i,0] = - v[i-1] # A_phi[0, i] = v[i-1]
				module_weak_popov(A_phi)

				# A_phi = diagonal_matrix([g]*p)
				# A_phi[0,0] = PR(1)
				# for i in range(1,A_phi.ncols()):
				# 	A_phi[0, i] = -v[i-1] # A_phi[i,0] = - v[i-1] #
				# module_weak_popov(A_phi)

				# ---- try each row to find solution ----
				for i in range(0, A_phi.nrows()):
					deg_right = True
					a_row = A_phi.rows()[i]
					for j in range(A_phi.ncols()):
						if a_row[j].degree()> floor((r-j)/p):
							deg_right = False
							break
					if deg_right == False:
						deg_right = True
						continue
					sigma = sum([a_row[j]**p*(x**j) for j in range(len(a_row))])
					roots = sigma.roots()
					e_vec = vector(F_base, len(word))
					errloc = []
					for root_mul in roots:
						try:
							loc = L.index(root_mul[0])
						except ValueError:
							break
						val = F_base(phi * root_mul[1])
						e_vec[loc] = val
						errloc.append(loc)
					# print(errloc)
					if e_vec.dot_product(vector(SynCal)) == s:
						S.append(word - e_vec)
			return S

GoppaCode._registered_encoders["CommonEncoder"] = GoppaCodeCommonEncoder
GoppaCode._registered_decoders["PattersonDecoder"] = PattersonDecoder
GoppaCode._registered_decoders["GoppaSyndromeDecoder"] = GoppaSyndromeDecoder
GoppaCode._registered_decoders['IntGoppaSyndromeDecoder'] = IntGoppaSyndromeDecoder
GoppaCode._registered_decoders["RSSuperCodeDecoder"] = RSSuperCodeDecoder
GoppaCode._registered_decoders["RSSyndromeDecoder"] = RSSyndromeDecoder
GoppaCode._registered_decoders["IRSDecoder"] = IRS_Decoder
GoppaCode._registered_decoders["GeneralizedPattersonDecoder"] = GeneralizedPattersonDecoder
