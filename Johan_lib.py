# Copyright Johan S. R. Nielsen
# This file contains the necessary functions for Lattice Reduction in several decoders in Goppa_code.py
# The functions are copied from Johan_module.sage and Johan_utils.sage, which are parts of the codinglib for Sage.
# This codinglib is free software released under the GPL v3 or any later version as
# published by the Free Software Foundation.

def LP(v, weights=None):
    """If v is a vector of polynomials, return the leading position of v using
    <_w where w is the weights vector (0 is assumed as all weights if none
    given). In case of tie, the highest position is given"""
    if not weights:
        weights=[0]*len(v)
    best = None
    bestp = None
    for p in range(0,len(v)):
        if not v[p].is_zero():
            vpdeg = v[p].degree() + weights[p]
            if vpdeg >= best:
                best = vpdeg
                bestp = p
    if best == None:
        return -1
    else:
        return bestp

################################################################################
# Module minimization / Lattice basis reduction algorithms
################################################################################

def module_row_reduction(M, i, j, pos):
    """Perform a row reduction with row j on row i, reducing position
    pos. If M[i,pos] has lower degree than M[j,pos], nothing is changed.
    Returns the multiple of row j used."""
    pow = M[i,pos].degree() - M[j,pos].degree()
    if pow < 0:
        return None
    coeff = -M[i, pos].leading_coefficient() / M[j, pos].leading_coefficient()
    x = M.base_ring().gen()
    multiple = x**pow*coeff
    M.add_multiple_of_row(i, j, multiple)
    return x**pow*coeff


def module_mulders_storjohann(M, weights=None, debug=0):
    """Reduce $M$ to weak Popov form using the Mulders--Storjohann
    algorithm (Mulders, T., and A. Storjohann. "On Lattice Reduction
    for Polynomial Matrices." Journal of Symbolic Computation 35, no.
    4 (2003): 377-401.).

    Handles column weights with only a slight possible penalty. The weights can
    be fractions, but cannot be floating points.
    
    If debug is True, then print some info."""
    if weights and len(weights) != M.ncols():
        raise ValueError("The number of weights must equal the number of columns")
    # initialise conflicts list and LP map
    LP_to_row = dict( (i,[]) for i in range(M.ncols()))
    conflicts = []
    for i in range(M.nrows()):
        lp = LP(M.row(i), weights=weights)
        ls = LP_to_row[lp]
        ls.append(i)
        if len(ls) > 1:
            conflicts.append(lp)
    iters = 0
    # while there is a conflict, do a row reduction
    while conflicts:
        lp = conflicts.pop()
        ls = LP_to_row[lp]
        i, j = ls.pop(), ls.pop()
        if M[i,lp].degree() < M[j, lp].degree():
            j,i = i,j

        module_row_reduction(M, i, j, lp)
        ls.append(j)
        lp_new = LP(M.row(i), weights=weights)
        if lp_new > -1:
            ls_new = LP_to_row[lp_new]
            ls_new.append(i)
            if len(ls_new) > 1:
                conflicts.append(lp_new)
        iters += 1
    return iters


################################################################################
# Higher-level reduction algorithms
################################################################################

def module_weak_popov(M, weights=None, debug=0):
    """Compute a (possibly column-weighted) weak Popov form of $M$.
    The weights can be any non-negative fractions."""
    return module_mulders_storjohann(M, weights=weights, debug=debug)
